// Hey Emacs, this is -*- coding: utf-8 -*-

import WebSocket from 'ws';

import { createMachine, interpret } from 'xstate';

// import { inspect } from "@xstate/inspect";
import { inspect } from '@xstate/inspect/lib/server';

// inspect({
//   url: "https://statecharts.io/inspect",
//   iframe: false
// });

inspect({
  // https://github.com/statelyai/xstate/discussions/1795
  // https://statecharts.io/inspect?server=localhost:8888

  // https://github.com/statelyai/xstate-viz/issues/191
  // https://github.com/statelyai/xstate-viz/pull/293
  server: new WebSocket.Server({
    port: 8888,
  }),
});

// Stateless machine definition
// machine.transition(...) is a pure function used by the interpreter.
const toggleMachine = createMachine({
  id: 'toggleMachine',
  initial: 'inactive',
  states: {
    inactive: {
      on: { TOGGLE: 'active' },
    },
    active: {
      on: { TOGGLE: 'inactive' },
    },
  },
});

// Machine instance with internal state
const toggleService = interpret(toggleMachine, { devTools: true })
  .onTransition((state) => console.log(state.value))
  .start();

// toggleService.send('TOGGLE');
// toggleService.send('TOGGLE');
